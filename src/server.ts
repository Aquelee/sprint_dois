import express, { request, response } from "express";
import { createServer } from "http";
import { Server, Socket } from "socket.io";

import "./database/index";
import { routes } from "./routes";

const app = express();

const http = createServer(app); // criando o protocolo http
const io = new Server(http);    //criando o protocolo websocket

app.use(express.json());

app.use(routes);

app.listen(3333, () => console.log("Server is running on port 3333"));
